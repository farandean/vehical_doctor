package com.example.vehicledoctor.Adaptor;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicledoctor.Activities.ItemDetailActivity;
import com.example.vehicledoctor.Model.SparePartsModel;
import com.example.vehicledoctor.R;

import java.util.ArrayList;
import java.util.List;


public class MechanicalShopItemsAdapter extends RecyclerView.Adapter<MechanicalShopItemsAdapter.ViewHolder> {
    Context context;
    List<SparePartsModel> sparePartsModels = new ArrayList<>();


    public MechanicalShopItemsAdapter(Context context, List<SparePartsModel> guradianModelsList) {
        this.context = context;
        this.sparePartsModels = guradianModelsList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spare_parts_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        final SparePartsModel sparePartsModel = sparePartsModels.get(position);
        holder.itemName.setText(sparePartsModel.getProduct_name());
        holder.itemDescription.setText(sparePartsModel.getDiscription());
        holder.itemPrice.setText(sparePartsModel.getFixed_price());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ItemDetailActivity.class);

                context.startActivity(intent);
            }
        });
        holder.icWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ItemDetailActivity.class);

                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return sparePartsModels.size();
    }

    private void initView() {


    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        private CardView cardview;
        private LinearLayout layoutItem;
        private ImageView image1;
        private LinearLayout layoutItemDesc;
        private ImageView icWishlist;
        private TextView itemName;
        private TextView itemDescription;
        private TextView itemPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardview = itemView.findViewById(R.id.cardview);
            layoutItem = itemView.findViewById(R.id.layout_item);
            image1 = itemView.findViewById(R.id.image1);
            icWishlist = itemView.findViewById(R.id.ic_wishlist);
            itemName = itemView.findViewById(R.id.item_name);
            itemDescription = itemView.findViewById(R.id.item_description);
            itemPrice = itemView.findViewById(R.id.item_price);
        }

    }

}
