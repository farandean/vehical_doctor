package com.example.vehicledoctor.Adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicledoctor.Model.SparePartsModel;
import com.example.vehicledoctor.R;

import java.util.ArrayList;
import java.util.List;


public class CartListItemsAdapter extends RecyclerView.Adapter<CartListItemsAdapter.ViewHolder> {
    Context context;
    List<SparePartsModel> sparePartsModels = new ArrayList<>();


    public CartListItemsAdapter(Context context, List<SparePartsModel> sparePartsModels) {
        this.context = context;
        this.sparePartsModels = sparePartsModels;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cartlist_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        SparePartsModel data = sparePartsModels.get(position);
        holder.itemDescription.setText(data.getDiscription());
        holder.itemName.setText(data.getProduct_name());
        holder.itemPrice.setText(data.getFixed_price());


    }


    @Override
    public int getItemCount() {
        return sparePartsModels.size();
    }

    private void initView() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        private ImageView imageCartlist;
        private LinearLayout layoutItemDesc;
        private TextView itemName;
        private TextView itemDescription;
        private TextView itemPrice;
        private LinearLayout layoutRemove;
        private LinearLayout layoutEdit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageCartlist = itemView.findViewById(R.id.image_cartlist);
            layoutItemDesc = itemView.findViewById(R.id.layout_item_desc);
            itemName = itemView.findViewById(R.id.item_name);
            itemDescription = itemView.findViewById(R.id.item_description);
            itemPrice = itemView.findViewById(R.id.item_price);
            layoutRemove = itemView.findViewById(R.id.layout_remove);
            layoutEdit = itemView.findViewById(R.id.layout_edit);
        }

    }

}
