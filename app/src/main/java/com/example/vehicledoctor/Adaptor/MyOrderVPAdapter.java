package com.example.vehicledoctor.Adaptor;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.vehicledoctor.Activities.Fragment.DeliveredFragment;
import com.example.vehicledoctor.Activities.Fragment.PendingFragment;
import com.example.vehicledoctor.Activities.Fragment.RejectedFragment;
import com.example.vehicledoctor.R;


public class MyOrderVPAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.pending, R.string.delivered, R.string.rejected};
    private final Context mContext;

    public MyOrderVPAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PendingFragment();
            case 1:
                return new DeliveredFragment();
            case 2:
                return new RejectedFragment();

        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 3;
    }
}