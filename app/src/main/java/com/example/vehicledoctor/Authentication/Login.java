package com.example.vehicledoctor.Authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vehicledoctor.Activities.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import static android.text.Html.fromHtml;

import com.example.vehicledoctor.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {
    private TextView tv_create_Account;
    private String sEmail, sPassword;
    private EditText edit_text;
    private FirebaseAuth mAuth;
    private ProgressDialog progress_Dialog;
    private Button mBtn_Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(Login.this, MainActivity.class));
            finish();
        }
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mBtn_Login = (Button) findViewById(R.id.btn_login_signIN);
        tv_create_Account = findViewById(R.id.tv_login_create_Account);
        tv_create_Account.setText(fromHtml("<font color='#ffffff'>I don't have account yet. </font><font color='#00b8d4'>Create One</font>"));
        tv_create_Account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Registration.class));
            }
        });
        progress_Dialog = new ProgressDialog(Login.this);
        mBtn_Login.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View v) {
                                              progress_Dialog.setMessage("Please Wait......");
                                              progress_Dialog.setTitle("Authentication");
                                              progress_Dialog.setCancelable(false);
                                              progress_Dialog.setCanceledOnTouchOutside(false);
                                              progress_Dialog.show();
                                              edit_text = findViewById(R.id.et_login_email);
                                              sEmail = edit_text.getText().toString();
                                              edit_text = findViewById(R.id.et_login_password);
                                              sPassword = edit_text.getText().toString();
                                              if (sEmail.matches("") || sPassword.matches("")) {
                                                  progress_Dialog.dismiss();
                                                  Toast.makeText(Login.this, "Complete All fields....!!!",
                                                          Toast.LENGTH_LONG).show();
                                              } else {
                                                  mAuth.signInWithEmailAndPassword(sEmail, sPassword).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                                                      @Override
                                                      public void onComplete(@NonNull Task<AuthResult> task) {
                                                          if (!task.isSuccessful()) {
                                                              try {
                                                                  throw task.getException();
                                                              } catch (Exception e) {
                                                                  progress_Dialog.dismiss();
                                                                  Toast.makeText(Login.this, "Login Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                                              }
                                                          } else {
                                                              String key = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                                              DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(key);
                                                              databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                  @Override
                                                                  public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                      progress_Dialog.dismiss();
                                                                      if (dataSnapshot.exists()) {
                                                                          if (dataSnapshot.child("status").getValue().toString().compareTo("unblock") == 0) {
                                                                              Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                                              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                              startActivity(intent);
                                                                              Toast.makeText(Login.this, "Successfully Login....!!!",
                                                                                      Toast.LENGTH_LONG).show();
                                                                              finish();
                                                                          } else {
                                                                              FirebaseAuth.getInstance().signOut();
                                                                              Toast.makeText(Login.this, "You Are Blocked From Admin....!!!",
                                                                                      Toast.LENGTH_LONG).show();
                                                                          }
                                                                      }
                                                                  }

                                                                  @Override
                                                                  public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                      progress_Dialog.dismiss();
                                                                      FirebaseAuth.getInstance().signOut();
                                                                      Toast.makeText(Login.this, "Login Failed....!!!\n" + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                                                                  }
                                                              });

                                                          }
                                                      }
                                                  });
                                              }

                                          }
                                      }
        );
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
