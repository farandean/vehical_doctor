package com.example.vehicledoctor.Authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vehicledoctor.Activities.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import com.example.vehicledoctor.R;

public class Registration extends AppCompatActivity {
    private Button mSubmit;
    private String sEmail, sPassword, sConfirmPass, sCnic, sName, sPhone, sGender;
    private EditText edit_text;
    private FirebaseAuth mAuth;
    private String[] maleRoute_array, femaleRoute_array;
    private ProgressDialog progress_Dialog;
    private RadioGroup rg_gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mAuth = FirebaseAuth.getInstance();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mSubmit = (Button) findViewById(R.id.btn_signUp_reg_user);
        rg_gender = findViewById(R.id.rg_signUp_gender);
        sGender = "Male";
        rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb.getText().toString().compareToIgnoreCase("Male") == 0) {
                    sGender = "Male";
                } else {
                    sGender = "Female";
                }

            }
        });

        //For register Account
        progress_Dialog = new ProgressDialog(Registration.this);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                progress_Dialog.setTitle("Create New Account");
                progress_Dialog.setMessage("Please Wait.....");
                progress_Dialog.show();
                edit_text = findViewById(R.id.et_signUp_user_email);
                sEmail = edit_text.getText().toString();
                edit_text = findViewById(R.id.et_signUp_user_pass);
                sPassword = edit_text.getText().toString();
                edit_text = findViewById(R.id.et_signUp_user_confirm_pass);
                sConfirmPass = edit_text.getText().toString();
                edit_text = findViewById(R.id.et_signUp_user_cnic_no);
                sCnic = edit_text.getText().toString();
                edit_text = findViewById(R.id.et_signUp_user_name);
                sName = edit_text.getText().toString();
                edit_text = findViewById(R.id.et_signUp_user_phone);
                sPhone = edit_text.getText().toString();
                if (sEmail.matches("") || sPassword.matches("") || sConfirmPass.matches("")
                        || sCnic.matches("") || sName.matches("") || sPhone.matches("")) {
                    progress_Dialog.dismiss();
                    Toast.makeText(Registration.this, "Complete All The Fields....!!!",
                            Toast.LENGTH_LONG).show();
                } else if (sPassword.compareTo(sConfirmPass) != 0) {
                    progress_Dialog.dismiss();
                    Toast.makeText(Registration.this, "Password doesn't match with Confirm Password....!!!",
                            Toast.LENGTH_LONG).show();
                } else {
                    mAuth.createUserWithEmailAndPassword(sEmail, sPassword).addOnCompleteListener(Registration.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                try {
                                    throw task.getException();
                                } catch (Exception e) {
                                    progress_Dialog.dismiss();
                                    Toast.makeText(Registration.this, "Failed Registration: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                String user_id = mAuth.getCurrentUser().getUid();
                                Map map = new HashMap();
                                map.put("name", sName);
                                map.put("phone", sPhone);
                                map.put("cnic", sCnic);
                                map.put("email", sEmail);
                                map.put("gender", sGender);
                                map.put("status", "unblock");
                                final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
                                databaseReference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            progress_Dialog.dismiss();
                                            Toast.makeText(Registration.this, "Successfully Account Created ....!!!", Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            progress_Dialog.dismiss();
                                            Toast.makeText(Registration.this, "Failed:\n" + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
