package com.example.vehicledoctor.Model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class SparePartsModel implements Parcelable {

    String id;
    String product_name;
    Bitmap product_image;
    String fixed_price;
    String discription;

    protected SparePartsModel(Parcel in) {
        id = in.readString();
        product_name = in.readString();
        product_image = in.readParcelable(Bitmap.class.getClassLoader());
        fixed_price = in.readString();
        discription = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(product_name);
        dest.writeParcelable(product_image, flags);
        dest.writeString(fixed_price);
        dest.writeString(discription);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SparePartsModel> CREATOR = new Creator<SparePartsModel>() {
        @Override
        public SparePartsModel createFromParcel(Parcel in) {
            return new SparePartsModel(in);
        }

        @Override
        public SparePartsModel[] newArray(int size) {
            return new SparePartsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Bitmap getProduct_image() {
        return product_image;
    }

    public void setProduct_image(Bitmap product_image) {
        this.product_image = product_image;
    }

    public String getFixed_price() {
        return fixed_price;
    }

    public void setFixed_price(String fixed_price) {
        this.fixed_price = fixed_price;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public SparePartsModel(String id, String product_name, Bitmap product_image, String fixed_price, String discription) {
        this.id = id;
        this.product_name = product_name;
        this.product_image = product_image;
        this.fixed_price = fixed_price;
        this.discription = discription;
    }
}
