package com.example.vehicledoctor.Model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

public class MechanicaModel implements Parcelable {
    private String id;
    public String name;
    public String address;
    public String phone;

    protected MechanicaModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        address = in.readString();
        phone = in.readString();
    }

    public static final Creator<MechanicaModel> CREATOR = new Creator<MechanicaModel>() {
        @Override
        public MechanicaModel createFromParcel(Parcel in) {
            return new MechanicaModel(in);
        }

        @Override
        public MechanicaModel[] newArray(int size) {
            return new MechanicaModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public MechanicaModel(String id, String name, String address, String phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(phone);
    }
}
