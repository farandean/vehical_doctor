package com.example.vehicledoctor.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;

import com.example.vehicledoctor.Adaptor.MyOrderVPAdapter;
import com.example.vehicledoctor.R;
import com.google.android.material.tabs.TabLayout;

public class MyOrdersActivity extends AppCompatActivity {

    TabLayout tabs;
    ViewPager viewPager;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setTitle("My Orders");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        context = MyOrdersActivity.this;
        MyOrderVPAdapter myOrderVPAdapter = new MyOrderVPAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(myOrderVPAdapter);
        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }
}
