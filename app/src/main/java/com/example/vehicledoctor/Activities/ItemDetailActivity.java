package com.example.vehicledoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vehicledoctor.Model.SparePartsModel;
import com.example.vehicledoctor.R;

public class ItemDetailActivity extends AppCompatActivity {

    private ImageView image1;
    private TextView itemName;
    private TextView itemPrice;
    private TextView textRatings;
    private TextView textRatingsReviews;
    private LinearLayout layoutActionShare;
    private TextView textAction1;
    private TextView textAction2;
    private LinearLayout layoutActionWishlist;
    private TextView textAction3;
    private TextView itemDescription;
    private TextView addCartBtn;
    private TextView buyNowBtn;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        initView();
        context = this;
        final SparePartsModel sparePartsModel = getIntent().getParcelableExtra("item");
        layoutActionShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ItemDetailActivity.this, "share", Toast.LENGTH_SHORT).show();
            }
        });
        layoutActionWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ItemDetailActivity.this, "add in wishlist", Toast.LENGTH_SHORT).show();
            }
        });
        addCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CartListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        buyNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ItemDetailActivity.this, "buy now", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        image1 = findViewById(R.id.image1);
        itemName = findViewById(R.id.item_name);
        itemPrice = findViewById(R.id.item_price);
        textRatings = findViewById(R.id.text_ratings);
        textRatingsReviews = findViewById(R.id.text_ratings_reviews);
        layoutActionShare = findViewById(R.id.layout_action_share);

        layoutActionWishlist = findViewById(R.id.layout_action_wishlist);
        itemDescription = findViewById(R.id.item_description);
        addCartBtn = findViewById(R.id.add_cart_btn);
        buyNowBtn = findViewById(R.id.buy_now_btn);
    }
}
