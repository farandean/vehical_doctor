package com.example.vehicledoctor.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.vehicledoctor.R;

public class MyWishListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wish_list);
    }
}
