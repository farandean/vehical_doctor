package com.example.vehicledoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vehicledoctor.R;

public class ShopDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView shopname;
    private TextView ownerName;
    private TextView phoneNo;
    private TextView address;
    private TextView email;
    private View view;
    private Button viewProducts;
    private Button map;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);
        initView();
        String id = getIntent().getStringExtra("id");
        context = this;
        if (id != null) {

        }
        viewProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MechanicalItemsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        imageView = findViewById(R.id.imageView);
        shopname = findViewById(R.id.shop_name);
        ownerName = findViewById(R.id.owner_name);
        phoneNo = findViewById(R.id.phone_no);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email);
        view = findViewById(R.id.view);
        viewProducts = findViewById(R.id.view_products);
        map = findViewById(R.id.map);
    }
}
