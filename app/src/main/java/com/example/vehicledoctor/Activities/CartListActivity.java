package com.example.vehicledoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicledoctor.Adaptor.CartListItemsAdapter;
import com.example.vehicledoctor.Model.SparePartsModel;
import com.example.vehicledoctor.R;

import java.util.ArrayList;
import java.util.List;

import static com.example.vehicledoctor.Activities.MainActivity.notificationCountCart;

public class CartListActivity extends AppCompatActivity {

    private LinearLayout activityCartList;
    private LinearLayout layoutItems;
    private RecyclerView recyclerview;
    private LinearLayout layoutPayment;
    private ImageView ivEmptyStates;
    private TextView tvInfo;
    private Button bAddNew;
    Context context;
    List<SparePartsModel> list;
    private TextView rsBtn;
    private TextView payment;
    LinearLayout layoutCartItems;
    LinearLayout layoutCartNoItems;
    LinearLayout layoutCartPayments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        initView();
        context = this;
        list = new ArrayList<>();


        setCartLayout();
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.image_8);

        SparePartsModel sparePartsModel = new SparePartsModel("1", "product name", icon, "100",
                "Item description");
        for (int i = 0; i < 1; i++) {
            notificationCountCart = 1;
            layoutCartNoItems.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
            layoutCartPayments.setVisibility(View.VISIBLE);
            list.add(sparePartsModel);
        }

        recyclerview.setAdapter(new CartListItemsAdapter(context, list));

    }


    private void initView() {
        activityCartList = findViewById(R.id.activity_cart_list);
        layoutItems = findViewById(R.id.layout_items);
        recyclerview = findViewById(R.id.recyclerview);
        layoutPayment = findViewById(R.id.layout_payment);
        ivEmptyStates = findViewById(R.id.ivEmptyStates);
        tvInfo = findViewById(R.id.tvInfo);
        bAddNew = findViewById(R.id.bAddNew);
        rsBtn = findViewById(R.id.rs_btn);
        payment = findViewById(R.id.payment);
    }

    protected void setCartLayout() {

        layoutCartPayments = (LinearLayout) findViewById(R.id.layout_payment);
        layoutCartItems = (LinearLayout) findViewById(R.id.layout_items);
        layoutCartNoItems = (LinearLayout) findViewById(R.id.layout_cart_empty);

        if (notificationCountCart > 0) {
            layoutCartNoItems.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
            layoutCartPayments.setVisibility(View.VISIBLE);
        } else {
            layoutCartNoItems.setVisibility(View.VISIBLE);
            layoutCartItems.setVisibility(View.GONE);
            layoutCartPayments.setVisibility(View.GONE);

            Button bStartShopping = (Button) findViewById(R.id.bAddNew);
            bStartShopping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        }
    }

}
