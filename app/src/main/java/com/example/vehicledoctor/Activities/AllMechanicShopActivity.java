package com.example.vehicledoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicledoctor.Adaptor.MechanicalShopAdapter;
import com.example.vehicledoctor.Model.MechanicaModel;
import com.example.vehicledoctor.R;

import java.util.ArrayList;
import java.util.List;

public class AllMechanicShopActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private MechanicalShopAdapter mAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_mechanics);
        context = this;
        recyclerView = findViewById(R.id.recycler_view);

        List<MechanicaModel> items = new ArrayList<>();
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.image_8);

        MechanicaModel sparePartsModel = new MechanicaModel("1", "shop name", "item description", "03015306545");
        for (int i = 0; i < 15; i++) {
            items.add(sparePartsModel);
        }
        mAdapter = new MechanicalShopAdapter(this, items);

        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new MechanicalShopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, MechanicaModel obj, int position) {
                Intent intent = new Intent(context, ShopDetailActivity.class);
                intent.putExtra("id", obj.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
}
