package com.example.vehicledoctor.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vehicledoctor.Adaptor.MechanicalShopItemsAdapter;
import com.example.vehicledoctor.Model.SparePartsModel;
import com.example.vehicledoctor.R;

import java.util.ArrayList;
import java.util.List;

public class MechanicalItemsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    MechanicalShopItemsAdapter mAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mechanical_items);
        context = this;

        recyclerView = findViewById(R.id.recycler_view);
        List<SparePartsModel> items = new ArrayList<>();
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.image_8);

        SparePartsModel sparePartsModel = new SparePartsModel("1", "product name", icon, "100",
                "Item description");
        for (int i = 0; i < 15; i++) {
            items.add(sparePartsModel);
        }


        mAdapter = new MechanicalShopItemsAdapter(this, items);

        recyclerView.setAdapter(mAdapter);


    }
}
