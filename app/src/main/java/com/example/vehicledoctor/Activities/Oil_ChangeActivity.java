package com.example.vehicledoctor.Activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.example.vehicledoctor.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * import com.guna.ocrlibrary.OCRCapture;
 * <p>
 * import static com.guna.ocrlibrary.OcrCaptureActivity.TextBlockObject;
 **/

public class Oil_ChangeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_set_new_meter, btn_verify;
    private EditText editText;
    private ProgressDialog progressDialog;
    private Long old_meterReading;
    private final int CAMERA_SCAN_TEXT = 131;
    private final int LOAD_IMAGE_RESULTS = 1;
    //private ImageButton btn_camera;
    private String user_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oil__change);
        user_key = FirebaseAuth.getInstance().getCurrentUser().getUid();
        old_meterReading = null;
        /*btn_camera=(ImageButton) findViewById(R.id.btn_camera);
        btn_camera.setOnClickListener(this);*/
        editText = (EditText) findViewById(R.id.et_new_meter_reading);
        btn_set_new_meter = (Button) findViewById(R.id.btn_new_Reading);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        btn_verify.setOnClickListener(this);
        btn_set_new_meter.setOnClickListener(this);
        progressDialog = new ProgressDialog(Oil_ChangeActivity.this);
        progressDialog.setMessage("Please Wait ......");
        progressDialog.setTitle("Verifying");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        loading_meterReading();
    }

    private void loading_meterReading() {
        progressDialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Meter Reading").child(user_key).child("reading");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    old_meterReading = Long.parseLong(dataSnapshot.getValue().toString());
                    progressDialog.dismiss();
                } else {
                    old_meterReading = null;
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                old_meterReading = null;
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_set_new_meter) {
            set_meter_reading();
        } else if (v == btn_verify) {
            String reading = editText.getText().toString();
            if (reading.matches("")) {
                editText.setError("Please Enter Meter Reading");
                Toast.makeText(this, "Please Enter Meter Reading", Toast.LENGTH_SHORT).show();
            } else {
                verifying_Meter_Reading(reading);
            }
        } /*else if(v==btn_camera)
        { OCRCapture.Builder(this)
                .setUseFlash(true)
                .setAutoFocus(true)
                .buildWithRequestCode(CAMERA_SCAN_TEXT);
        }*/
    }

    private void set_meter_reading() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Oil_ChangeActivity.this);
        View view = Oil_ChangeActivity.this.getLayoutInflater().inflate(R.layout.new_meter_reading, null);
        Button btn_Ok = view.findViewById(R.id.btn_set_meter_reading);
        Button btn_cancel = view.findViewById(R.id.btn_cancel_meter_dialog);
        final EditText et_set_meterReading = view.findViewById(R.id.et_set_meter_reading);
        TextView tv_save_meter = view.findViewById(R.id.tv_Old_meterReading);
        if (old_meterReading != null) {
            tv_save_meter.setText(old_meterReading.toString());
        } else {
            tv_save_meter.setText("Nothing Available");
        }
        builder.setView(view);
        final AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_set_meterReading.getText().toString().matches("")) {
                    et_set_meterReading.setError("Enter Meter Reading!!!");
                } else {
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Meter Reading").child(user_key).child("reading");
                    databaseReference.setValue(et_set_meterReading.getText().toString());
                    loading_meterReading();
                    alert.dismiss();
                }
            }
        });
    }

    private void notification_create(String msg) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(Oil_ChangeActivity.this, "Vehicle Doctor")
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Meter Calculation")
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, mBuilder.build());
    }

    private void verifying_Meter_Reading(final String meterReading) {
        if (old_meterReading != null) {
            long new_reading = Long.parseLong(meterReading);
            new_reading = new_reading - old_meterReading;
            String msg = "";
            if (new_reading < 0) {
                msg = "Your new Meter Reading is less then Old Meter Reading It should be Greater \nNew  Meter Reading:-  " + Long.parseLong(meterReading)
                        + "\nSave Meter Reading:-  " + old_meterReading;
                alert_Dialog(msg);
            } else if (new_reading < 2500) {
                msg = "you have a limit for changing Oil...\n" + new_reading + " KM Traveled in this oil energy";
                alert_Dialog(msg);
            } else if (new_reading >= 2500 && new_reading < 3000) {
                Long remaining = 3000 - new_reading;
                msg = "Warning for changing Oil... You have only \n" + remaining + " KM Remaining";
                alert_Dialog(msg);
                notification_create(msg);
            } else {
                msg = "You Exceed your Limit \n Please Change Your Oil\nyou are Traveled " + new_reading + " KM";
                alert_Dialog(msg);
                notification_create(msg);
            }
        } else {
            String msg = "You Have n't Old Meter Reading First Please set Your Meter Reading";
            alert_Dialog(msg);
            progressDialog.dismiss();
        }

    }

    private void alert_Dialog(String msg) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(Oil_ChangeActivity.this, AlertDialog.THEME_HOLO_DARK);
        builder.setTitle("Oil Change Detail !!!")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_menu_info_details)
                .show();
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionCamera:
                //Scan text from camera.
                OCRCapture.Builder(this)
                        .setUseFlash(false)
                        .setAutoFocus(true)
                        .buildWithRequestCode(CAMERA_SCAN_TEXT);
                break;
            case R.id.actionPhoto:
                Intent intentGallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, LOAD_IMAGE_RESULTS);
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
          /*  if (requestCode == CAMERA_SCAN_TEXT) {
                if (resultCode == CommonStatusCodes.SUCCESS) {
                    editText.setText(data.getStringExtra(TextBlockObject));
                }

            } else if (requestCode == LOAD_IMAGE_RESULTS) {
                Uri pickedImage = data.getData();
                //Extract text from image.
                String text = OCRCapture.Builder(this).getTextFromUri(pickedImage);
                //You can also use getTextFromBitmap(Bitmap bitmap) or getTextFromImage(String imagePath) buplic APIs from ibrary.
                editText.setText(text);

            }*/
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(Oil_ChangeActivity.this, MainActivity.class));
        //overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }
}

