package com.example.vehicledoctor.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.vehicledoctor.Authentication.Login;
import com.example.vehicledoctor.Helper.NotificationCountSetClass;
import com.example.vehicledoctor.R;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout llMainAllMechanics;
    private LinearLayout llMainNearbyMechanics;
    private LinearLayout llMainOilChanger;
    private LinearLayout llMainOrders;
    private LinearLayout llMainStores;
    private LinearLayout llMainLogout;

    public static int notificationCountCart = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initView();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        llMainAllMechanics.setOnClickListener(this);
        llMainNearbyMechanics.setOnClickListener(this);
        llMainOilChanger.setOnClickListener(this);
        llMainOrders.setOnClickListener(this);
        llMainStores.setOnClickListener(this);
        llMainLogout.setOnClickListener(this);
    }

    private void initView() {
        llMainAllMechanics = (LinearLayout) findViewById(R.id.ll_main_all_mechanics);
        llMainNearbyMechanics = (LinearLayout) findViewById(R.id.ll_main_nearby_mechanics);
        llMainOilChanger = (LinearLayout) findViewById(R.id.ll_main_oil_changer);
        llMainOrders = (LinearLayout) findViewById(R.id.ll_main_orders);
        llMainStores = (LinearLayout) findViewById(R.id.ll_main_stores);
        llMainLogout = (LinearLayout) findViewById(R.id.ll_main_logout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_main_all_mechanics: {
                Intent intent = new Intent(MainActivity.this, AllMechanicShopActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.ll_main_nearby_mechanics: {
                Toast.makeText(this, "Nearby Mechanics", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.ll_main_oil_changer: {
                Toast.makeText(this, "Oil Change", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, Oil_ChangeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.ll_main_orders: {
                Intent intent = new Intent(MainActivity.this, MyOrdersActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.ll_main_stores: {
                Intent intent = new Intent(MainActivity.this, StoresActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.ll_main_logout: {
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    FirebaseAuth.getInstance().signOut();
                }
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Get the notifications MenuItem and
        // its LayerDrawable (layer-list)
        MenuItem item = menu.findItem(R.id.action_cart);
        NotificationCountSetClass.setAddToCart(MainActivity.this, item, notificationCountCart);
        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

           /* NotificationCountSetClass.setAddToCart(MainActivity.this, item, notificationCount);
            invalidateOptionsMenu();*/
            startActivity(new Intent(MainActivity.this, CartListActivity.class));

           /* notificationCount=0;//clear notification count
            invalidateOptionsMenu();*/
            return true;
        } else {
//            startActivity(new Intent(MainActivity.this, EmptyActivity.class));

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
